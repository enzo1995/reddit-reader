//
//  CustomTextField.swift
//  Reddit reader
//
//  Created by enzo corsiero on 11/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import SwiftUI

struct CustomTextField: View {
    
    var placeHolder: String
    @Binding var text: String
    
    var body: some View {
        VStack {
            TextField(placeHolder, text: self.$text)
                .padding(.horizontal, 30).padding(.top, 20)
                .font(FontManager.sharedInstance.boldFontWith(size: Constants.TextSize.Description))
            Divider()
                .padding(.horizontal, 30)
                .foregroundColor(.black)
        }
    }
}
