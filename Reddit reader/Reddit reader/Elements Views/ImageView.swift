//
//  ImageView.swift
//  Reddit reader
//
//  Created by enzo corsiero on 09/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import Foundation

import SwiftUI

struct ImageView: View {
    var url: URL
    var size: CGFloat
    var placeHolder: String = String.empty
    var id: String?
    
    var body: some View {
        URLImage(url: url, id: id, placeholder: ActivityIndicator(isAnimating: .constant(true), style: .medium)
)
            .aspectRatio(contentMode: .fill)
            .frame(width: size, height: size)
            .cornerRadius(8)
            .overlay(
                RoundedRectangle(cornerRadius: 8)
                    .stroke(Color(.sRGB, red: 150/255, green: 150/255, blue: 150/255, opacity: 0.1), lineWidth: 1)
        )
            .shadow(radius: 1)
    }
}

#if DEBUG
struct ImageView_Previews: PreviewProvider {
    static var previews: some View {
        ImageView(url: URL(string: "https://i.redd.it/u9978fz2vnt41.jpg")!, size: 90)
    }
}
#endif
