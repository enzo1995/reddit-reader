//
//  MainButtonStyle.swift
//  Reddit reader
//
//  Created by enzo corsiero on 11/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import SwiftUI

struct MainButtonStyle: ButtonStyle {
    
  func makeBody(configuration: Self.Configuration) -> some View {
    configuration.label
        .font(FontManager.sharedInstance.boldFontWith(size: Constants.TextSize.ButtonTitle))
        .foregroundColor(Color.white)
        .padding()
        .frame(maxWidth: .infinity, maxHeight: 30, alignment: .center)
        .contentShape(Rectangle())
        .background(LinearGradient(gradient: Gradient(colors: [Color.red, Color.orange]), startPoint: .leading, endPoint: .trailing))
        .scaleEffect(configuration.isPressed ? 1.3 : 1.0)
        .cornerRadius(15.0)
  }
}
