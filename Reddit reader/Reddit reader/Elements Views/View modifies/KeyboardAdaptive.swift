//
//  BaseTextField.swift
//  Reddit reader
//
//  Created by enzo corsiero on 11/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import SwiftUI
import Combine

struct KeyboardAdaptive: ViewModifier {
    @State private var keyboardHeight: CGFloat = 0

    func body(content: Content) -> some View {
        content
            .padding(.bottom, keyboardHeight)
            .onReceive(Publishers.keyboardHeight) { self.keyboardHeight = $0 }
            .animation(.easeInOut(duration: 0.2))
    }
}
