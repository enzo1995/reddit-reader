//
//  CustomText.swift
//  Reddit reader
//
//  Created by enzo corsiero on 11/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit

struct BaseText: ViewModifier {
    var color: Color = Color(UIColor(named: "textColor") ?? .black)
    var fontType: FontType = .regular
    var size: CGFloat = Constants.TextSize.Description
    
    func body(content: Content) -> some View {
        return content
            .font(FontManager.sharedInstance.fontBy(type: fontType, size: size))
            .foregroundColor(color)
            .padding([.leading, .trailing], 16)
            .padding(.top, 8)
    }
}
