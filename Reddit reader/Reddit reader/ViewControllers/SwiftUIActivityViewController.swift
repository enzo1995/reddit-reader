//
//  SwiftUIActivityViewController.swift
//  Reddit reader
//
//  Created by enzo corsiero on 14/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

struct SwiftUIActivityViewController : UIViewControllerRepresentable {

    let activityViewController = ActivityViewController()

    func makeUIViewController(context: Context) -> ActivityViewController {
        activityViewController
    }
    func updateUIViewController(_ uiViewController: ActivityViewController, context: Context) {
        //
    }
    func shareImage(uiImage: UIImage) {
        activityViewController.uiImage = uiImage
        activityViewController.shareImage()
    }
}
