//
//  HomeView.swift
//  Reddit reader
//
//  Created by enzo corsiero on 10/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import Combine
import SwiftUI

var sub: Cancellable? = nil

struct HomeView: View {
    
    @State private var topic: String = String.empty
    @State private var toastText: String = String.empty
    @State var showToast = false
    @State var pushToGridImages = false
    
    @State var data: [ResponseModel<ImagesCollection>] = []
    
    @State var fromCache = false
    
    var body: some View {
        
        SceneDelegate.shared?.window!.overrideUserInterfaceStyle = .light
        
        return ZStack {
            GeometryReader { geometry in
                NavigationView {
                    ZStack(alignment: .top) {
                        Image("backgroundImage")
                            .resizable()
                            .scaledToFill()
                            .edgesIgnoringSafeArea(.all)
                            .frame(minWidth: 0, maxWidth: geometry.size.width)
                        
                        Text(Localizer.HomeView.welcomeText.localized)
                            .modifier(BaseText(fontType: .medium, size: Constants.TextSize.Title))
                            .multilineTextAlignment(.center)
                            .foregroundColor(.black)
                        
                        VStack {
                            Spacer()
                            VStack {
                                CustomTextField(placeHolder: Localizer.HomeView.topicPlaceHolder.localized, text: self.$topic)
                                
                                Button(action: {
                                    if self.topic.isEmpty {
                                        self.toastText = Localizer.HomeView.emptyTopicDescription.localized
                                        self.showToast = true
                                    } else {
                                        self.getData()
                                    }
                                }) {
                                    Text(Localizer.HomeView.searchButton.localized)
                                }.buttonStyle(MainButtonStyle())
                                    .padding(16)
                                
                            }
                        .frame(maxWidth: 400)
                            .background(Color.alphaGray)
                            .cornerRadius(8.0)
                            .keyboardAdaptive()
                            
                            NavigationLink(destination: ImagesGridView(imagesList: self.data, topic: self.topic, fromCache: self.fromCache), isActive: self.$pushToGridImages) {
                                EmptyView()
                            }
                            Spacer()
                        }.padding( 20)
                            .navigationBarTitle(Localizer.Common.title.localized)
                        
                    }
                }.accentColor(.mainColor)
                    .toast(isShowing: self.$showToast, text:
                        Text(self.toastText)
                )
            }.navigationViewStyle(StackNavigationViewStyle())
        }
    }
    
    private func getData() {
        UIApplication.shared.endEditing()
        LoaderMessage.shared.showLoading()
        sub = RedditAPI.top(keyWord: self.topic)
            .print()
            .sink(receiveCompletion: { result in
                switch(result) {
                case .failure:
                    do {
                        if let data = try FilesManager().getResponse(keyword: self.topic) {
                            let value = try JSONDecoder().decode(ResponseModel<DataModel>.self, from: data)
                            self.fromCache = true
                            self.goToImagesGrid(response: value)
                        } else {
                            self.toastText = Localizer.Error.connection.localized
                            self.showToast = true
                        }
                    } catch {
                        self.toastText = Localizer.Error.connection.localized
                        self.showToast = true
                    }
                case .finished:
                    break
                }
                LoaderMessage.shared.stopLoading()
            },
                  receiveValue: {
                    self.fromCache = false
                    self.goToImagesGrid(response: $0)
            })
    }
    
    private func goToImagesGrid(response: ResponseModel<DataModel>) {
        guard let data = response.data?.children?.filter({($0.data?.url.containsImageUrl ?? false)}), !data.isEmpty else {
            self.toastText = Localizer.Error.dataNotFound.localized
            self.showToast = true
            return
        }
        self.data = data
        self.pushToGridImages = true
    }
}

#if DEBUG
struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
#endif
