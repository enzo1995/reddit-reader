//
//  ContentView.swift
//  Reddit reader
//
//  Created by enzo corsiero on 09/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import SwiftUI

struct ImagesGridView: View {
    
    @State var showingDetail = false
    @State var detailData: ImagesCollection!
    
    @State private var toastText: String = String.empty
    @State var showToast = false
    
    @State private var searchText : String = ""
    
    let imagesList: [ResponseModel<ImagesCollection>]
    let topic: String
    
    let fromCache: Bool
    
    var body: some View {
        
        SceneDelegate.shared?.window!.overrideUserInterfaceStyle = .unspecified
        
        return VStack {
            SearchBar(text: $searchText, placeholder: Localizer.ImagesGrid.searchBarPlaceholder.localized)
            filterItems()
                .sheet(isPresented: self.$showingDetail) {
                    ImageDetailView(isShowing: self.$showingDetail, presenting: self, data: self.detailData)
            }
            Spacer()
        }.navigationBarTitle(topic).foregroundColor(.mainColor)
            .onAppear {
                if self.fromCache {
                    self.toastText = Localizer.ImagesGrid.cachedData.localized
                    self.showToast = true
                }
        }
        .toast(isShowing: self.$showToast, text:
            Text(self.toastText)
        )
    }
    
    private func addImageView(collection: ResponseModel<ImagesCollection>) -> AnyView {
        if let stringUrl = collection.data?.url, let url = URL(string: stringUrl) {
            return AnyView(
                Button(action: {
                    if let data = collection.data {
                        self.detailData = data
                        self.showingDetail = true
                    }
                }) {
                    ImageView(url: url, size: 90, id: collection.data?.id)
                }
                .buttonStyle(PlainButtonStyle())
            )
        } else {
            return AnyView(EmptyView())
        }
    }
    
    private func filterItems() -> AnyView {
        let filteredItems = self.imagesList.filter({
            self.searchText.isEmpty ||
                $0.data?.name.contains(self.searchText) ?? false ||
                $0.data?.title.contains(self.searchText) ?? false
        })
        if filteredItems.isEmpty {
            return AnyView(
                ZStack(alignment: .center) {                    Text(Localizer.ImagesGrid.searchNoData.localized)
                    .multilineTextAlignment(.center)
                    .padding()
                }
            )
        } else {
            return AnyView(
                ScrollView(.vertical) {
                    Grid(filteredItems, id: \.data?.id) { collection in
                        self.addImageView(collection: collection)
                    }.gridStyle(
                        StaggeredGridStyle(tracks: .min(100))
                    )
                }
            )
        }
    }
    
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        var imageList: [ResponseModel<ImagesCollection>] = []
        for i in 0..<5 {
            let imageCollection = ImagesCollection(id: "\(i)", url: "https://i.redd.it/u9978fz2vnt41.jpg", title: "", name: "", author: "autore", total_awards_received: 0, created: 0, all_awardings: [])
            let collection = ResponseModel(kind: "", data: imageCollection)
            imageList.append(collection)
        }
        return ImagesGridView(imagesList: [], topic: "test", fromCache: false)
    }
}
#endif
