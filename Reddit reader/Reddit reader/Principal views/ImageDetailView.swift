//
//  ImageDetailView.swift
//  Reddit reader
//
//  Created by enzo corsiero on 10/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import SwiftUI

struct ImageDetailView<Presenting>: View where Presenting: View {
    
    @Binding var isShowing: Bool
    let presenting: Presenting
    let data: ImagesCollection
    
    let activityViewController = SwiftUIActivityViewController()
    
    var body: some View {
        ScrollView(.vertical) {
            HStack(alignment: .bottom) {
                VStack(alignment: .center, spacing: 24) {
                    
                    self.imageView()
                    
                    self.addAwards()
                    
                    Divider()
                    
                    Text(self.data.title)
                        .modifier(BaseText(fontType: .medium, size: Constants.TextSize.Subtitle))
                        .padding([.top, .bottom], 16)
                        .multilineTextAlignment(.center)
                    
                    HStack {
                        Text(Localizer.ImageDetail.name.localized)
                            .modifier(BaseText(fontType: .bold))
                        Text(self.data.name)
                            .modifier(BaseText())
                    }
                    
                    HStack {
                        Text(Localizer.ImageDetail.author.localized)
                            .modifier(BaseText(fontType: .bold))
                        Text(self.data.author)
                            .modifier(BaseText())
                    }
                    
                    Text(String(format: Localizer.ImageDetail.loadedDate.localized, Date(timeIntervalSince1970: self.data.created).dateString()))
                        .modifier(BaseText())
                    
                    
                }
            }
        }.frame(alignment: .top)
    }
    
    private func imageView() -> AnyView {
        if let url = URL(string: self.data.url) {
            let imageView = URLImage(url: url, id: self.data.id, force: true, placeholder: ActivityIndicator(isAnimating: .constant(true), style: .medium))
            return AnyView(
                VStack {
                    imageView
                        .aspectRatio(contentMode: .fill)
                        .frame(minHeight: 200, alignment: .center)
                    
                    Spacer(minLength: 12)
                    
                    Button(action: {
                        if let image = imageView.loader.image {
                            self.activityViewController.shareImage(uiImage: image)
                        }
                    }) {
                        ZStack{
                            Text(Localizer.ImageDetail.share.localized)
                            self.activityViewController
                        }
                    }
                    .buttonStyle(MainButtonStyle())
                    .padding()
                    Divider()
            })
        } else {
            return AnyView(EmptyView())
        }
    }
    
    private func addAwards() -> AnyView {
        func addAward(urlString: String, id: String?) -> AnyView {
            if let url = URL(string: urlString) {
                return AnyView(ImageView(url: url, size: 25, id: id))
            } else {
                return AnyView(EmptyView())
            }
        }
        
        if self.data.all_awardings.isEmpty {
            return AnyView(
                Text(Localizer.ImageDetail.noRewards.localized)
                    .modifier(BaseText())
            )
        } else {
            return AnyView(
                VStack(alignment: .leading) {
                    Text(Localizer.ImageDetail.awards.localized)
                        .modifier(BaseText(fontType: .bold))
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack(alignment: .center, spacing: 8) {
                            ForEach(self.data.all_awardings, id: \.id) { award in
                                addAward(urlString: award.icon_url, id: award.id)
                            }
                        }.padding([.trailing, .leading, .top], 8)
                    }
                }
            )
        }
    }
    
    
}

#if DEBUG
struct ImageDetailView_Previews: PreviewProvider {
    @State static var value = false
    
    static var previews: some View {
        let imageCollection = ImagesCollection(id: "1", url: "https://i.redd.it/u9978fz2vnt41.jpg", title: "Titolo", name: "Nome", author: "Autore", total_awards_received: 0, created: 0, all_awardings: [])
        return ImageDetailView(isShowing: $value, presenting: HomeView(), data: imageCollection)
    }
}
#endif
