//
//  CustomAlert.swift
//  Reddit reader
//
//  Created by enzo corsiero on 10/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import SwiftUI

struct Toast<Presenting>: View where Presenting: View {
    
    /// The binding that decides the appropriate drawing in the body.
    @Binding var isShowing: Bool
    /// The view that will be "presenting" this toast
    let presenting: () -> Presenting
    /// The text to show
    let text: Text
    
    let delay: TimeInterval = 3
    
    var body: some View {
        if isShowing {
            DispatchQueue.main.asyncAfter(deadline: .now() + self.delay) {
                withAnimation {
                    self.isShowing = false
                }
            }
            UINotificationFeedbackGenerator().notificationOccurred(.warning)
        }
        
        return GeometryReader { geometry in
            
            ZStack(alignment: .bottom) {
                
                self.presenting()
                    .blur(radius: self.isShowing ? 1 : 0)
                
                VStack(alignment: .center) {
                    self.text.multilineTextAlignment(.center)
                        .modifier(BaseText(color: .black, fontType: .bold))
                        .padding()
                }
                .frame(width: geometry.size.width * 4 / 5,
                       height: 120)
                    .background(Color.alphaGray)
                    .foregroundColor(Color.primary)
                    .cornerRadius(20)
                    .transition(.scale)
                    .position(x: geometry.size.width / 2, y: self.isShowing ? geometry.size.height - 100 : geometry.size.height + 200)
                
            }
        }
        
    }
    
}
