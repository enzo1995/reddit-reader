//
//  Localizer.swift
//  Reddit reader
//
//  Created by enzo corsiero on 10/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import Foundation

public extension Localizable {
    var localizedKey: String { return "reddit" }
}

public enum Localizer {
    
    public enum Common: String, Localizable {
        case title
    }
    
    public enum HomeView: String, Localizable {
        case topicPlaceHolder
        case emptyTopicTitle
        case emptyTopicDescription
        case searchButton
        case welcomeText
    }
    
    public enum ImageDetail: String, Localizable {
        case noRewards
        case loadedDate
        case author
        case name
        case awards
        case share
    }
    
    public enum ImagesGrid: String, Localizable {
        case searchBarPlaceholder
        case searchNoData
        case cachedData
    }
    
    public enum Image: String, Localizable {
        case loader
    }
    
    public enum Error: String, Localizable {
        case connection
        case dataNotFound
    }
}
