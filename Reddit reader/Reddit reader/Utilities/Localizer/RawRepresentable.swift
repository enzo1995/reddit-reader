//
//  RawRepresentable.swift
//  Reddit reader
//
//  Created by enzo corsiero on 10/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import Foundation

public protocol Localizable {
    var localizedKey: String { get }
}

public extension RawRepresentable where RawValue == String, Self: Localizable {
    var localized: String {
        let key = "\(localizedKey).\(String(describing: type(of: self)).lowercased()).\(self)"
        return NSLocalizedString(key, tableName: nil, bundle: Bundle.main, value: String.empty, comment: String.empty)
    }
}
