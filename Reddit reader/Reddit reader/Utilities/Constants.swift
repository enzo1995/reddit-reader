//
//  Constants.swift
//  Reddit reader
//
//  Created by enzo corsiero on 10/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    struct FontName {
        static let regular = "Roboto-Regular"
        static let medium = "Roboto-Medium"
        static let bold = "Roboto-Bold"
    }
    
    struct DateFormat {
        static let Date = "dd/MM/yyyy"
    }
    
    struct TextSize {
        static let SuperTitle: CGFloat = 38.0
        static let Title: CGFloat = 24.0
        static let Subtitle: CGFloat = 18.0
        
        static let Description: CGFloat = 16.0
        static let SubDescription: CGFloat = 14.0
        
        static let ButtonTitle: CGFloat = 18.0
    }
}
