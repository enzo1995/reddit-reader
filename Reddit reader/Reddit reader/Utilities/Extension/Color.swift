//
//  Color.swift
//  Reddit reader
//
//  Created by enzo corsiero on 10/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import Foundation
import SwiftUI

extension Color {
    
    static let mainColor = Color.mainRed
    
    // MARK: - Red color
    static let mainRed = Color(red: 247 / 255, green: 67 / 255, blue: 0 / 255)
    
    // MARK: - gray color
    static let alphaGray = Color(red: 190 / 255, green: 190 / 255, blue: 190 / 255).opacity(0.8)
    
}
