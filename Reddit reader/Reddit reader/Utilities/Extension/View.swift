//
//  View.swift
//  Reddit reader
//
//  Created by enzo corsiero on 10/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import SwiftUI

extension View {

    func toast(isShowing: Binding<Bool>, text: Text) -> some View {
        Toast(isShowing: isShowing,
              presenting: { self },
              text: text)
    }
    
    func keyboardAdaptive() -> some View {
        ModifiedContent(content: self, modifier: KeyboardAdaptive())
    }

}
