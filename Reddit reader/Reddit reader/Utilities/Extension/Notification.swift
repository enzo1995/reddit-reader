//
//  Notification.swift
//  Reddit reader
//
//  Created by enzo corsiero on 11/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import Foundation
import UIKit

extension Notification {
    var keyboardHeight: CGFloat {
        return (userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect)?.height ?? 0
    }
}
