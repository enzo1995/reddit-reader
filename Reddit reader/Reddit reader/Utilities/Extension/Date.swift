//
//  Date.swift
//  Reddit reader
//
//  Created by enzo corsiero on 11/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import Foundation

extension Date {
    
    func dateString() -> String {
        self.toString(format: Constants.DateFormat.Date)
    }
    
    func toString(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = NSLocale(localeIdentifier: Locale.preferredLanguages[0]) as Locale
        return formatter.string(from: self)
    }
}
