//
//  UISceen.swift
//  Reddit reader
//
//  Created by enzo corsiero on 14/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import UIKit

extension UIScreen{
   static let screenWidth = UIScreen.main.bounds.size.width
   static let screenHeight = UIScreen.main.bounds.size.height
   static let screenSize = UIScreen.main.bounds.size
}
