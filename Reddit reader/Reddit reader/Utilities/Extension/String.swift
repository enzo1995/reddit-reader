//
//  String.swift
//  Reddit reader
//
//  Created by enzo corsiero on 10/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import Foundation

public extension String {
    static var empty: String { "" }
    
    var containsImageUrl: Bool {
        let imageFormts = [".gif", ".jpg", ".jpeg", ".png", ".tiff", ".tif"]
        for format in imageFormts where self.contains(format) {
            return true
        }
        return false
    }
}
