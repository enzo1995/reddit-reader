//
//  FontManager.swift
//  Reddit reader
//
//  Created by enzo corsiero on 10/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import Foundation
import SwiftUI

enum FontType {
    case medium
    case regular
    case bold
}

final class FontManager {
    
    static let sharedInstance = FontManager()
    
    private init() {}
    
    let regularFontName: String = Constants.FontName.regular
    
    let boldFontName: String = Constants.FontName.bold
    
    let mediumFontName: String = Constants.FontName.medium
        
    func regularFontWith(size: CGFloat) -> Font {
        Font.custom(regularFontName, size: size)
    }
    
    func boldFontWith(size: CGFloat) -> Font {
        Font.custom(boldFontName, size: size)
    }
    
    func mediumFontWith(size: CGFloat) -> Font {
        Font.custom(mediumFontName, size: size)
    }
    
    func fontBy(type: FontType, size: CGFloat) -> Font {
        switch type {
        case .regular:
            return regularFontWith(size: size)
        case .medium:
            return mediumFontWith(size: size)
        case .bold:
            return boldFontWith(size: size)
        }
    }
}
