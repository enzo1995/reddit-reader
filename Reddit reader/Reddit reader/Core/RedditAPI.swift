//
//  RedditAPI.swift
//  Reddit reader
//
//  Created by enzo corsiero on 10/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import UIKit
import Combine

enum RedditAPI {
    static let agent = Agent()
    static let base = URL(string: "https://www.reddit.com")!
}

extension RedditAPI {
    
    static func top(keyWord: String) -> AnyPublisher<ResponseModel<DataModel>, Error> {
        let request = URLRequest(url: base.appendingPathComponent("r/\(keyWord)/top.json"))
        return agent.run(request)
            .map(\.value)
            .eraseToAnyPublisher()
    }
}

struct ResponseModel<T: Codable>: Codable {
    let kind: String?
    let data: T?
}

struct DataModel: Codable {
    let modhash: String?
    let dist: Int?
    let children: [ResponseModel<ImagesCollection>]?
    let after: String?
    let before: String?
}
