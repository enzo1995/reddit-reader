//
//  Agent.swift
//  Reddit reader
//
//  Created by enzo corsiero on 10/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import Combine
import UIKit

struct Agent {
    
    struct Response<T> {
        let value: T
        let response: URLResponse
    }
    
    func run<T: Decodable>(_ request: URLRequest, _ decoder: JSONDecoder = JSONDecoder()) -> AnyPublisher<Response<T>, Error> {
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 5.0
        sessionConfig.timeoutIntervalForResource = 5.0
        let session = URLSession(configuration: sessionConfig)
        
        let cancellable = session
            .dataTaskPublisher(for: request)
            .tryMap { result -> Response<T> in
                let value = try decoder.decode(T.self, from: result.data)
                if let components = request.url?.pathComponents, components.count > 1 {
                    let key = components[components.count - 2]
                    try FilesManager().saveResponse(keyword: key, data: result.data)
                }
                return Response(value: value, response: result.response)
        }
        .receive(on: DispatchQueue.main)
        .retry(2)
        .eraseToAnyPublisher()
        return cancellable
    }
}
