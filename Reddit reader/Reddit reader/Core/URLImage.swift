//
//  URLImage.swift
//  Reddit reader
//
//  Created by enzo corsiero on 09/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

struct URLImage<Placeholder: View>: View {
    @ObservedObject var loader: ImageLoader
    private let placeholder: Placeholder?
        
    init(url: URL, id: String? = nil, force: Bool = false, placeholder: Placeholder? = nil) {
        loader = ImageLoader(url: url, id: id, forceLoad: force)
        self.placeholder = placeholder
    }

    var body: some View {
        image
            .onAppear(perform: loader.load)
            .onDisappear(perform: loader.cancel)
    }
    
    private var image: some View {
        Group {
            if loader.image != nil {
                Image(uiImage: loader.image!)
                    .resizable()
            } else {
                placeholder
            }
        }
    }
}

final class ImageLoader: ObservableObject {
    @Published var image: UIImage?
    private let url: URL
    private let id: String?
    private let forceLoad: Bool
    
    private var cancellable: AnyCancellable?
        
    init(url: URL, id: String?, forceLoad: Bool) {
        self.url = url
        self.id = id
        self.forceLoad = forceLoad
    }
    
    deinit {
        cancellable?.cancel()
    }
    
    func load() {
        guard image == nil else { return }
        ImageCache.shared.image(for: id) { image in
            if let image = image, !self.forceLoad {
                DispatchQueue.main.async {
                    self.image = image
                }
            } else {
                let sessionConfig = URLSessionConfiguration.default
                sessionConfig.timeoutIntervalForRequest = 5
                sessionConfig.timeoutIntervalForResource = 5
                let session = URLSession(configuration: sessionConfig)
                self.cancellable = session.dataTaskPublisher(for: self.url)
                    .map {
                        let loadImage = UIImage(data: $0.data)
                        if image == nil {
                            ImageCache.shared.insertImage(loadImage, for: self.id)
                        }
                        DispatchQueue.main.async {
                            self.image = loadImage
                        }
                }
                    .receive(on: DispatchQueue.main)
                    .sink(receiveCompletion: { result in
                        switch(result) {
                        case .failure:
                            if let image = image {
                                self.image = image
                            }
                            break
                        case .finished:
                            break
                        }
                    },receiveValue: {})}
        }
    }
    
    func cancel() {
        cancellable?.cancel()
    }
}
