//
//  FilesManager.swift
//  Reddit reader
//
//  Created by enzo corsiero on 13/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import Foundation

class FilesManager {
    enum Error: Swift.Error {
        case fileAlreadyExists
        case invalidDirectory
        case writtingFailed
        case readingFiled
        case fileNotExist
    }
    let fileManager: FileManager
    init(fileManager: FileManager = .default) {
        self.fileManager = fileManager
    }
    func save(fileNamed: String, data: Data) throws {
        guard let url = makeURL(forFileNamed: fileNamed) else {
            throw Error.invalidDirectory
        }
        do {
            try data.write(to: url)
        } catch {
            throw Error.writtingFailed
        }
    }
    
    private func makeURL(forFileNamed fileName: String) -> URL? {
        guard let url = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return nil
        }
//        let directory = url.appendingPathComponent("Images")
//        let folderExists = (try? directory.checkResourceIsReachable()) ?? false
//        if !folderExists {
//            try? fileManager.createDirectory(at: directory, withIntermediateDirectories: false)
//        }
        return url.appendingPathComponent(fileName)
    }
    
    func get(fileNamed: String) throws -> Data? {
        guard let url = makeURL(forFileNamed: fileNamed) else {
            throw Error.invalidDirectory
        }
        guard fileManager.fileExists(atPath: url.path) else {
            throw Error.fileNotExist
        }
        do {
            return try Data(contentsOf: url)
        } catch {
            throw Error.readingFiled
        }
        
    }
    func saveResponse(keyword: String, data: Data) throws {
        guard let url = makeResponseURL(forFileNamed: keyword) else {
            throw Error.invalidDirectory
        }
        do {
            try data.write(to: url)
        } catch {
            throw Error.writtingFailed
        }
    }
    
    private func makeResponseURL(forFileNamed fileName: String) -> URL? {
        guard let url = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return nil
        }
        let directory = url.appendingPathComponent("Responses")
        let folderExists = (try? directory.checkResourceIsReachable()) ?? false
        if !folderExists {
            try? fileManager.createDirectory(at: directory, withIntermediateDirectories: false)
        }
        return directory.appendingPathComponent(fileName)
    }
    
    func getResponse(keyword: String) throws -> Data? {
        guard let url = makeResponseURL(forFileNamed: keyword) else {
            throw Error.invalidDirectory
        }
        guard fileManager.fileExists(atPath: url.path) else {
            throw Error.fileNotExist
        }
        do {
            return try Data(contentsOf: url)
        } catch {
            throw Error.readingFiled
        }
        
    }
    
}
