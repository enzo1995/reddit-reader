//
//  ImageCache.swift
//  Reddit reader
//
//  Created by enzo corsiero on 11/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import Foundation
import UIKit

final class ImageCache {
    
    static let shared = ImageCache()
    
    init() {
        opsQue.maxConcurrentOperationCount = 1
    }
    
    let opsQue = OperationQueue()
    
    func saveImageFile(id: String, image: UIImage) {
        let op = BlockOperation {
            do {
                if let data = image.jpeg(.lowest) {
                    try FilesManager().save(fileNamed: id, data: data)
                }
            } catch {
                print("Couldn't write file")
            }
        }
        opsQue.addOperation(op)
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func getImageFile(id: String, completion: @escaping (UIImage?) -> Void) {
        let op = BlockOperation {
            do {
                if let data = try FilesManager().get(fileNamed: id), let image = UIImage(data: data) {
                    completion(image)
                } else {
                    completion(nil)
                }
            } catch {
               completion(nil)
            }
        }
        opsQue.addOperation(op)
    }
}

extension ImageCache: ImageCacheType {
    func image(for id: String?, completion: @escaping (UIImage?) -> Void) {
        guard let id = id else {
        completion(nil)
            return
        }
        self.getImageFile(id: id, completion: completion)
    }
    
    func insertImage(_ image: UIImage?, for id: String?) {
        guard let image = image, let id = id else { return }
        self.saveImageFile(id: id, image: image)
    }
    
}


protocol ImageCacheType: class {
    func image(for id: String?, completion: @escaping (UIImage?) -> Void)
    func insertImage(_ image: UIImage?, for id: String?)
}
