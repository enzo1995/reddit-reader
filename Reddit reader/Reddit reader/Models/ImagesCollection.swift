//
//  ImageItem.swift
//  Reddit reader
//
//  Created by enzo corsiero on 09/07/2020.
//  Copyright © 2020 enzo corsiero. All rights reserved.
//

import Foundation


struct ImagesCollection: Identifiable, Codable {
    let id: String
    let url: String
    let title: String
    let name: String
    let author: String
    let total_awards_received: Int
    let created: TimeInterval
    let all_awardings: [Awards]
}

struct Awards: Codable {
    let id: String
    let icon_url: String
    let description: String
    let resized_icons: [ResizedIcon]
}

struct ResizedIcon: Codable {
    let url: String
    let width: Int
    let height: Int
}
